import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';


@Directive({
  selector: '[customValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting:  CustomValidatorDirective, multi: true}]
})

export class  CustomValidatorDirective implements Validator {
  @Input() customValidator: string = null;

  validate(control: AbstractControl): {[key: string]: any} {
    return this.IfFirstIsUppercaseValidator(control);
  }

  IfFirstIsUppercaseValidator(control: AbstractControl): {[key: string]: any}{
    if (control.value === null || (<string>control.value).length === 0) return null;
    
    let firstChar = control.value.substring(0, 1);
    if (firstChar !== firstChar.toUpperCase()) {
      return {'customValidator':  control.value};
    }
    return null;
  }
}

