import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-error-blocks',
  templateUrl: './error-blocks.component.html',
  styles: [`
    .cap {
      text-transform: capitalize;
    }
  `]
})
export class ErrorBlocksComponent implements OnInit {

  @Input() element;
  constructor() { }

  ngOnInit() {
  }

}
