import { Component } from '@angular/core';

class User {
  name: string;
  lastname: string;
  email: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: User;
  forbiddenName = '';

  constructor() {
    this.user = {
      name: '',
      lastname: '',
      email: '',
    }
  }
}
