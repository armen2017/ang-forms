import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CustomValidatorDirective } from './shared/custom-validator/custom-validator.component';
import { ErrorBlocksComponent } from './shared/error-blocks/error-blocks.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CustomValidatorDirective,
    ErrorBlocksComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
